﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

/// <summary>
/// Classe to authenticate a user with Google services
/// </summary>
public class GoogleAuthentication : MonoBehaviour
{
	private static string HTTP_CLIENT_LOGIN = "https://www.google.com/accounts/ClientLogin";
	private static string HTTP_OAUTH2_LOGIN = "https://accounts.google.com/o/oauth2/";
	private static string OAUTH2_AUTH_SERVICE = "auth";
	private static string OAUTH2_TOKEN_SERVICE = "token";
	
	/// <summary>
	/// Callback for the client login method.
	/// </summary>
	public delegate void ClientLoginCallback(bool success, string message, string SID, string LSID, string auth);
	
	/// <summary>
	/// Callback for the Oauth2 auth request.
	/// </summary>
	public delegate void OAuth2TokenCallback(bool success, string message, string accessToken, string tokenType, long expiresIn, string refreshToken);
	
	/// <summary>
	/// Method to login a user with the Google client login service. (more informations at https://developers.google.com/accounts/docs/AuthForInstalledApps)
	/// </summary>
	/// <param name='accountType'>
	/// Account type.
	/// </param>
	/// <param name='email'>
	/// The user email.
	/// </param>
	/// <param name='password'>
	/// The user password.
	/// </param>
	/// <param name='service'>
	/// The service name to login.
	/// </param>
	/// <param name='source'>
	/// The request source.
	/// </param>
	/// <param name='callback'>
	/// Callback.
	/// </param>
	public void ClientLogin(string accountType, string email, string password, string service, string source, ClientLoginCallback callback)
    {
	    string url = HTTP_CLIENT_LOGIN + "?accountType=" + accountType + "&Email=" + email + "&Passwd=" + password + "&service=" + service + "&source=" + source;
	    WWW request = new WWW(url);
		
		StartCoroutine(WaitForRequest(request, callback));
    }
		
	/// <summary>
	/// Waits for client login request.
	/// </summary>
	/// <returns>
	/// The request.
	/// </returns>
	/// <param name='request'>
	/// Request.
	/// </param>
	/// <param name='callback'>
	/// Callback.
	/// </param>
	private IEnumerator WaitForRequest(WWW request, ClientLoginCallback callback)
    {
        yield return request;
		
		// Init result
		bool success = false;
		string message = null;
		string SID = null;
		string LSID = null;
		string auth = null;
		
		if (String.IsNullOrEmpty(request.error))
		{
			string[] split = request.text.Split('\n');
	        foreach (string s in split)
	        {
	            string[] nvsplit = s.Split('=');
	            if (nvsplit.Length == 2)
	            {
					switch (nvsplit[0])
					{
						case "SID":
							SID = nvsplit[1];
						break;
						
						case "LSID":
							LSID = nvsplit[1];
						break;
						
						case "Auth":
							auth = nvsplit[1];
						break;
					}
	            }
	        }
			
			if (SID != null || LSID != null || auth != null)
			{
				success = true;
			}
			else
			{
				success = false;
				message = "Unknown Error";
			}
		}
		else
		{
			success = false;
			message = request.error;
		}
		
		callback(success, message, SID, LSID, auth);
	}
	
	/// <summary>
	/// Method to retrieve an auth2 code. (more informations at https://developers.google.com/accounts/docs/OAuth2)
	/// </summary>
	/// <param name='responseType'>
	/// Response type.
	/// </param>
	/// <param name='clientId'>
	/// Application client identifier.
	/// </param>
	/// <param name='redirectUri'>
	/// Redirect URI.
	/// </param>
	/// <param name='scope'>
	/// Scope.
	/// </param>
	/// <param name='callback'>
	/// Callback.
	/// </param>
	public void OAuth2Code(string responseType, string clientId, string redirectUri, string scope)
    {
		string url = HTTP_OAUTH2_LOGIN + OAUTH2_AUTH_SERVICE +
			"?response_type=" + responseType +
			"&client_id=" + clientId + 
			"&redirect_uri=" + redirectUri +
			"&scope=" + scope;
		
		Application.OpenURL(url);
    }
	
	/// <summary>
	/// Method to retrieve an auth2 token to log a user. (more informations at https://developers.google.com/accounts/docs/OAuth2)
	/// </summary>
	/// <param name='code'>
	/// OAuth2 Code.
	/// </param>
	/// <param name='clientId'>
	/// Application client identifier.
	/// </param>
	/// <param name='clientSecret'>
	/// Application client secret.
	/// </param>
	/// <param name='redirectUri'>
	/// Redirect URI.
	/// </param>
	/// <param name='grantType'>
	/// Grant type.
	/// </param>
	/// <param name='scope'>
	/// Scope.
	/// </param>
	/// <param name='callback'>
	/// Callback.
	/// </param>
	public void OAuth2Token(string code, string clientId, string clientSecret, string redirectUri, string grantType, string scope, OAuth2TokenCallback callback)
    {
		// Create a Web Form
	    WWWForm form = new WWWForm();
	    form.AddField("code", code);
		form.AddField("client_id", clientId);
		form.AddField("client_secret", clientSecret);
		form.AddField("redirect_uri", redirectUri);
		form.AddField("grant_type", grantType);
		form.AddField("scope", scope);
		
	    WWW request = new WWW(HTTP_OAUTH2_LOGIN + OAUTH2_TOKEN_SERVICE, form);
		
		StartCoroutine(WaitForRequest(request, callback));
    }
	
	/// <summary>
	/// Waits for OAuth2 token request.
	/// </summary>
	/// <returns>
	/// The request.
	/// </returns>
	/// <param name='request'>
	/// Request.
	/// </param>
	/// <param name='callback'>
	/// Callback.
	/// </param>
	private IEnumerator WaitForRequest(WWW request, OAuth2TokenCallback callback)
    {
        yield return request;
		
		// Init result
		bool success = false;
		string message = null;
		string accessToken = null;
		string tokenType = null;
		long expiresIn = 0; 
		string refreshToken = null;
		
		
		if (String.IsNullOrEmpty(request.error))
		{
			Dictionary<string, object> requestResult = MiniJSON.Json.Deserialize(request.text) as Dictionary<string, object>;
			accessToken  = (string)requestResult["access_token"];
			tokenType 	 = (string)requestResult["token_type"];
			expiresIn 	 = (long)requestResult["expires_in"];
			refreshToken = (string)requestResult["refresh_token"];
			
			success = true;
		}
		else
		{
			success = false;
			message = request.error;
		}
		
		callback(success, message, accessToken, tokenType, expiresIn, refreshToken);
	}
	
#if UNITY_ANDROID
	/// <summary>
	/// Method to log a user on Android platform with the GetAuthTokenByFeatures method of the account manager. (more informations at http://developer.android.com/reference/android/accounts/AccountManager.html)
	/// </summary>
	/// <param name='accountType'>
	/// Account type.
	/// </param>
	/// <param name='authTokenType'>
	/// Auth token type.
	/// </param>
	/// <param name='methodNameCallback'>
	/// Method name callback.
	/// </param>
	/// <param name='gameObjectCallback'>
	/// Game object to receive the callback.
	/// </param>
	public void GetAuthTokenByFeatures(string accountType, string authTokenType, string methodNameCallback, GameObject gameObjectCallback)
	{
		AndroidJavaClass playerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject activity = playerClass.GetStatic<AndroidJavaObject>("currentActivity");
		AndroidJavaClass pluginClass = new AndroidJavaClass("com.google.unity.AndroidAccountManagerUnity");
			
		pluginClass.CallStatic("getAuthTokenByFeatures", new object[5] { activity, accountType, authTokenType, methodNameCallback, gameObjectCallback.name });
	}
#endif
}