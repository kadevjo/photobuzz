﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Collections;

/// <summary>
/// Google Cloud Print plugin.
/// </summary>
public class GoogleCloudPrintPlugin : MonoBehaviour 
{
	private static string HTTP_URL = "https://www.google.com/cloudprint/";
	private static string SEARCH_SERVICE = "search";
	private static string SUBMIT_SERVICE = "submit";
	private static string JOBS_SERVICE = "jobs";
	private static string DELETEJOB_SERVICE = "deletejob";
	private static string PRINTER_SERVICE = "printer";
	
	public enum GCPAuthorizationType
	{
		OAuth,
		GoogleLogin
	}
	
	/// <summary>
	/// Gets or sets the type of the authorization.
	/// </summary>
	/// <value>
	/// The type of the authorization.
	/// </value>
	public GCPAuthorizationType AuthorizationType
	{
		get;
		set;
	}
	
	/// <summary>
	/// Gets or sets the app proxy.
	/// </summary>
	/// <value>
	/// The app proxy.
	/// </value>
	public string AppProxy
	{
		get;
		set;
	}
	
	/// <summary>
	/// Search call back.
	/// </summary>
	public delegate void SearchCallBack(bool success, string message, string response, List<GoogleCloudPrinter> printers);
	
	/// <summary>
	/// Submit call back.
	/// </summary>
	public delegate void SubmitCallBack(bool success, string message, string response);
	
	/// <summary>
	/// Jobs call back.
	/// </summary>
	public delegate void JobsCallBack(bool success, string message, string response, List<GoogleCloudJob> jobs);
	
	/// <summary>
	/// Delete job call back.
	/// </summary>
	public delegate void DeleteJobCallBack(bool success, string message, string response);
	
	/// <summary>
	/// Printer call back.
	/// </summary>
	public delegate void PrinterCallBack(bool success, string message, string response, GoogleCloudPrinter printer);
	
	/// <summary>
	/// Init params
	/// </summary>
	private void Awake()
	{
		// Set OAuth authorization type by default
		AuthorizationType = GCPAuthorizationType.OAuth;
		
		// Set app proxy
		AppProxy = "";
	}
	
	/// <summary>
	/// Method to create headers from scratch.
	/// </summary>
	/// <returns>
	/// The headers.
	/// </returns>
	/// <param name='authCode'>
	/// Auth code.
	/// </param>
	private Hashtable GetHeaders(string authCode)
	{
		return GetHeaders(new Hashtable(), authCode);
	}
	
	/// <summary>
	/// Method to complete existing headers.
	/// </summary>
	/// <returns>
	/// The headers.
	/// </returns>
	/// <param name='headers'>
	/// Headers.
	/// </param>
	/// <param name='authCode'>
	/// Auth code.
	/// </param>
	private Hashtable GetHeaders(Hashtable headers, string authCode)
	{
		headers["X-CloudPrint-Proxy"] = AppProxy;
			
		switch (AuthorizationType)
		{
			case GCPAuthorizationType.OAuth:
				headers["Authorization"] = "OAuth " + authCode;
			break;
			
			case GCPAuthorizationType.GoogleLogin:
				headers["Authorization"] = "GoogleLogin auth=" + authCode;
			break;
		}
		
		return headers;
	}
	
	/// <summary>
	/// The /search interface returns a list of printers accessible to the authenticated user, taking an optional search query as a parameter. Note that by default, /search will not include printers that have been offline for a long time (i.e. whose connectionStatus is DORMANT). This behavior can be overridden if ALL or DORMANT is passed as the value of the optional connection_status parameter. (more informations at https://developers.google.com/cloud-print/docs/appInterfaces)
	/// </summary>
	/// <param name='authCode'>
	/// Auth code.
	/// </param>
	/// <param name='callback'>
	/// Callback.
	/// </param>
	public void Search(string authCode, SearchCallBack callback)
	{
		Search(authCode, callback, false);
	}
	
	/// <summary>
	/// The /search interface returns a list of printers accessible to the authenticated user, taking an optional search query as a parameter. Note that by default, /search will not include printers that have been offline for a long time (i.e. whose connectionStatus is DORMANT). This behavior can be overridden if ALL or DORMANT is passed as the value of the optional connection_status parameter. (more informations at https://developers.google.com/cloud-print/docs/appInterfaces)
	/// </summary>
	/// <param name='authCode'>
	/// Auth code.
	/// </param>
	/// <param name='callback'>
	/// Callback.
	/// </param>
	/// <param name='storeResponse'>
	/// Store response. Specify if the response must be stored on the callback.
	/// </param>
	public void Search(string authCode, SearchCallBack callback, bool storeResponse)
	{
		// Add HTTP AUTH
		Hashtable headers = GetHeaders(authCode);
		
		// Call the service
	    WWW request = new WWW(HTTP_URL + SEARCH_SERVICE, null, headers);
		StartCoroutine(WaitForRequest(request, callback, storeResponse));
	}
	
	/// <summary>
	/// Waits for search request.
	/// </summary>
	/// <returns>
	/// The for request.
	/// </returns>
	/// <param name='request'>
	/// Request.
	/// </param>
	/// <param name='callback'>
	/// Callback.
	/// </param>
	/// <param name='storeResponse'>
	/// Store response. Specify if the response must be stored on the callback.
	/// </param>
	private IEnumerator WaitForRequest(WWW request, SearchCallBack callback, bool storeResponse)
    {
        yield return request;
		
		// Init data
		bool success = false;
		string message = null;
		string response = null;
		List<GoogleCloudPrinter> printers = null;
		
		if (String.IsNullOrEmpty(request.error))
		{
			// Get response
			string requestResponse = request.text;
			
			// Check result
			Dictionary<string, object> requestResult = MiniJSON.Json.Deserialize(requestResponse) as Dictionary<string, object>;
			success = (bool)requestResult["success"];
			
			// Init list of printers
			printers = new List<GoogleCloudPrinter>();
			
			if (success)
			{
				List<object> resultPrinters = (List<object>) requestResult["printers"];
				
				foreach (object printerObj in resultPrinters)
				{
					Dictionary<string, object> printerData = (Dictionary<string, object>) printerObj;
					
					// Add the printer
					printers.Add(GoogleCloudPrinter.Factory(printerData));
				}
			}
			
			if (storeResponse)
			{
				response = requestResponse;
			}
		}
		else
		{
			message = request.error;
		}
		
		// Call the callback
		callback(success, message, response, printers);
	}
	
	/// <summary>
	/// The /submit service interface is used to send print jobs to the GCP service. Upon initialization, the status of the print job will be QUEUED. The print job is created and the appropriate printer is notified of its existence. The status of the print job can then be tracked using /jobs. (more informations at https://developers.google.com/cloud-print/docs/appInterfaces)
	/// </summary>
	/// <param name='authCode'>
	/// Auth code.
	/// </param>
	/// <param name='printerId'>
	/// Printer identifier.
	/// </param>
	/// <param name='title'>
	/// Title.
	/// </param>
	/// <param name='capabilities'>
	/// Capabilities.
	/// </param>
	/// <param name='content'>
	/// Content.
	/// </param>
	/// <param name='contentType'>
	/// Content type.
	/// </param>
	/// <param name='tag'>
	/// Tag.
	/// </param>
	/// <param name='callback'>
	/// Callback.
	/// </param>
	public void Submit(string authCode, string printerId, string title, string capabilities, byte[] content, string contentType, string tag, SubmitCallBack callback)
	{
		Submit(authCode, printerId, title, capabilities, content, contentType, tag, callback, false);
	}
	
	/// <summary>
	/// The /submit service interface is used to send print jobs to the GCP service. Upon initialization, the status of the print job will be QUEUED. The print job is created and the appropriate printer is notified of its existence. The status of the print job can then be tracked using /jobs. (more informations at https://developers.google.com/cloud-print/docs/appInterfaces)
	/// </summary>
	/// <param name='authCode'>
	/// Auth code.
	/// </param>
	/// <param name='printerId'>
	/// Printer identifier.
	/// </param>
	/// <param name='title'>
	/// Title.
	/// </param>
	/// <param name='capabilities'>
	/// Capabilities.
	/// </param>
	/// <param name='content'>
	/// Content.
	/// </param>
	/// <param name='contentType'>
	/// Content type.
	/// </param>
	/// <param name='tag'>
	/// Tag.
	/// </param>
	/// <param name='callback'>
	/// Callback.
	/// </param>
	/// <param name='storeResponse'>
	/// Store response. Specify if the response must be stored on the callback.
	/// </param>
	public void Submit(string authCode, string printerId, string title, string capabilities, byte[] content, string contentType, string tag, SubmitCallBack callback, bool storeResponse)
	{
		// Create a Web Form
	    WWWForm form = new WWWForm();
	    form.AddField("printerid", printerId);
		form.AddField("title", title);
		form.AddField("capabilities", capabilities);
	    form.AddBinaryData("content", content, title, contentType);
		form.AddField("contentType", contentType);
		form.AddField("tag", tag);
		
		// Add HTTP AUTH
		Hashtable headers = GetHeaders(form.headers, authCode);
		
		// Call the service
	    WWW request = new WWW(HTTP_URL + SUBMIT_SERVICE, form.data, headers);
		StartCoroutine(WaitForRequest(request, callback, storeResponse));
	}
	
	/// <summary>
	/// Waits for submit request.
	/// </summary>
	/// <returns>
	/// The for request.
	/// </returns>
	/// <param name='request'>
	/// Request.
	/// </param>
	/// <param name='callback'>
	/// Callback.
	/// </param>
	/// <param name='storeResponse'>
	/// Store response. Specify if the response must be stored on the callback.
	/// </param>
	private IEnumerator WaitForRequest(WWW request, SubmitCallBack callback, bool storeResponse)
    {
        yield return request;
		
		// Init data
		bool success = false;
		string message = null;
		string response = null;
		
		if (String.IsNullOrEmpty(request.error))
		{
			// Get response
			string requestResponse = request.text;
			
			// Check result
			Dictionary<string, object> requestResult = MiniJSON.Json.Deserialize(requestResponse) as Dictionary<string, object>;
			success = (bool)requestResult["success"];
			message = (string)requestResult["message"];
			
			if (storeResponse)
			{
				response = requestResponse;
			}
		}
		else
		{
			message = request.error;
		}
		
		// Call the callback
		callback(success, message, response);
	}
	
	/// <summary>
	/// The /jobs interface retrieves the status of print jobs for a user. (more informations at https://developers.google.com/cloud-print/docs/appInterfaces)
	/// </summary>
	/// <param name='authCode'>
	/// Auth code.
	/// </param>
	/// <param name='callback'>
	/// Callback.
	/// </param>
	public void Jobs(string authCode, JobsCallBack callback)
	{
		Jobs (authCode, null, callback, false);
	}
	
	/// <summary>
	/// The /jobs interface retrieves the status of print jobs for a user. (more informations at https://developers.google.com/cloud-print/docs/appInterfaces)
	/// </summary>
	/// <param name='authCode'>
	/// Auth code.
	/// </param>
	/// <param name='callback'>
	/// Callback.
	/// </param>
	/// <param name='storeResponse'>
	/// Store response. Specify if the response must be stored on the callback.
	/// </param>
	public void Jobs(string authCode, JobsCallBack callback, bool storeResponse)
	{
		Jobs (authCode, null, callback, storeResponse);
	}
	
	/// <summary>
	/// The /jobs interface retrieves the status of print jobs for a user. (more informations at https://developers.google.com/cloud-print/docs/appInterfaces)
	/// </summary>
	/// <param name='authCode'>
	/// Auth code.
	/// </param>
	/// <param name='printerId'>
	/// Printer identifier.
	/// </param>
	/// <param name='callback'>
	/// Callback.
	/// </param>
	/// <param name='storeResponse'>
	/// Store response. Specify if the response must be stored on the callback.
	/// </param>
	public void Jobs(string authCode, string printerId, JobsCallBack callback, bool storeResponse)
	{
		// Create a Web Form
	    WWWForm form = new WWWForm();
	    form.AddField("printerid", printerId);
		
		// Add HTTP AUTH
		Hashtable headers = GetHeaders(form.headers, authCode);
		
		// Call the service
	    WWW request = new WWW(HTTP_URL + JOBS_SERVICE, form.data, headers);
		StartCoroutine(WaitForRequest(request, callback, storeResponse));
	}
	
	/// <summary>
	/// Waits for jobs request.
	/// </summary>
	/// <returns>
	/// The for request.
	/// </returns>
	/// <param name='request'>
	/// Request.
	/// </param>
	/// <param name='callback'>
	/// Callback.
	/// </param>
	/// <param name='storeResponse'>
	/// Store response. Specify if the response must be stored on the callback.
	/// </param>
	private IEnumerator WaitForRequest(WWW request, JobsCallBack callback, bool storeResponse)
    {
        yield return request;
		
		// Init data
		bool success = false;
		string message = null;
		string response = null;
		List<GoogleCloudJob> jobs = null;
		
		if (String.IsNullOrEmpty(request.error))
		{
			// Get response
			string requestResponse = request.text;
			
			// Check result
			Dictionary<string, object> requestResult = MiniJSON.Json.Deserialize(requestResponse) as Dictionary<string, object>;
			success = (bool)requestResult["success"];
			
			// Init list of printers
			jobs = new List<GoogleCloudJob>();
			
			if (success)
			{
				List<object> resultJobs = (List<object>) requestResult["jobs"];
				
				foreach (object jobObj in resultJobs)
				{
					Dictionary<string, object> jobData = (Dictionary<string, object>) jobObj;
					
					// Add the printer
					jobs.Add(GoogleCloudJob.Factory(jobData));
				}
			}
			
			if (storeResponse)
			{
				response = requestResponse;
			}
		}
		else
		{
			message = request.error;
		}
		
		// Call the callback
		callback(success, message, response, jobs);
	}
	
	/// <summary>
	/// The /deletejob interface deletes the given print job. (more informations at https://developers.google.com/cloud-print/docs/appInterfaces)
	/// </summary>
	/// <param name='authCode'>
	/// Auth code.
	/// </param>
	/// <param name='jobId'>
	/// Job identifier.
	/// </param>
	/// <param name='callback'>
	/// Callback.
	/// </param>
	public void DeleteJob(string authCode, string jobId, DeleteJobCallBack callback)
	{
		DeleteJob(authCode, jobId, callback, false);
	}
	
	/// <summary>
	/// The /deletejob interface deletes the given print job. (more informations at https://developers.google.com/cloud-print/docs/appInterfaces)
	/// </summary>
	/// <param name='authCode'>
	/// Auth code.
	/// </param>
	/// <param name='jobId'>
	/// Job identifier.
	/// </param>
	/// <param name='callback'>
	/// Callback.
	/// </param>
	/// <param name='storeResponse'>
	/// Store response. Specify if the response must be stored on the callback.
	/// </param>
	public void DeleteJob(string authCode, string jobId, DeleteJobCallBack callback, bool storeResponse)
	{
		// Create a Web Form
	    WWWForm form = new WWWForm();
	    form.AddField("jobid", jobId);
		
		// Add HTTP AUTH
		Hashtable headers = GetHeaders(form.headers, authCode);
		
		// Call the service
	    WWW request = new WWW(HTTP_URL + DELETEJOB_SERVICE, form.data, headers);
		StartCoroutine(WaitForRequest(request, callback, storeResponse));
	}
	
	/// <summary>
	/// Waits for deletejob request.
	/// </summary>
	/// <returns>
	/// The for request.
	/// </returns>
	/// <param name='request'>
	/// Request.
	/// </param>
	/// <param name='callback'>
	/// Callback.
	/// </param>
	/// <param name='storeResponse'>
	/// Store response. Specify if the response must be stored on the callback.
	/// </param>
	private IEnumerator WaitForRequest(WWW request, DeleteJobCallBack callback, bool storeResponse)
    {
        yield return request;
		
		// Init data
		bool success = false;
		string message = null;
		string response = null;
		
		if (String.IsNullOrEmpty(request.error))
		{
			// Get response
			string requestResponse = request.text;
			
			// Check result
			Dictionary<string, object> requestResult = MiniJSON.Json.Deserialize(requestResponse) as Dictionary<string, object>;
			success = (bool)requestResult["success"];
			message = (string)requestResult["message"];
			
			if (storeResponse)
			{
				response = requestResponse;
			}
		}
		else
		{
			message = request.error;
		}
		
		// Call the callback
		callback(success, message, response);
	}
	
	/// <summary>
	/// The /printer interface retrieves the capabilities of the specified printer. (more informations at https://developers.google.com/cloud-print/docs/appInterfaces)
	/// </summary>
	/// <param name='authCode'>
	/// Auth code.
	/// </param>
	/// <param name='printerId'>
	/// Printer identifier.
	/// </param>
	/// <param name='callback'>
	/// Callback.
	/// </param>
	public void Printer(string authCode, string printerId, PrinterCallBack callback)
	{
		Printer (authCode, printerId, false, callback, false);
	}
	
	/// <summary>
	/// The /printer interface retrieves the capabilities of the specified printer. (more informations at https://developers.google.com/cloud-print/docs/appInterfaces)
	/// </summary>
	/// <param name='authCode'>
	/// Auth code.
	/// </param>
	/// <param name='printerId'>
	/// Printer identifier.
	/// </param>
	/// <param name='printerConnectionStatus'>
	/// Printer connection status.
	/// </param>
	/// <param name='callback'>
	/// Callback.
	/// </param>
	/// <param name='storeResponse'>
	/// Store response. Specify if the response must be stored on the callback.
	/// </param>
	public void Printer(string authCode, string printerId, bool printerConnectionStatus, PrinterCallBack callback, bool storeResponse)
	{
		// Create a Web Form
	    WWWForm form = new WWWForm();
	    form.AddField("printerid", printerId);
		form.AddField("printer_connection_status", printerConnectionStatus.ToString());
		
		// Add HTTP AUTH
		Hashtable headers = GetHeaders(form.headers, authCode);
		
		// Call the service
	    WWW request = new WWW(HTTP_URL + PRINTER_SERVICE, form.data, headers);
		StartCoroutine(WaitForRequest(request, callback, storeResponse));
	}
	
	/// <summary>
	/// Waits for printer request.
	/// </summary>
	/// <returns>
	/// The for request.
	/// </returns>
	/// <param name='request'>
	/// Request.
	/// </param>
	/// <param name='callback'>
	/// Callback.
	/// </param>
	/// <param name='storeResponse'>
	/// Store response. Specify if the response must be stored on the callback.
	/// </param>
	private IEnumerator WaitForRequest(WWW request, PrinterCallBack callback, bool storeResponse)
    {
        yield return request;
		
		// Init data
		bool success = false;
		string message = null;
		string response = null;
		GoogleCloudPrinter printer = null;
		
		if (String.IsNullOrEmpty(request.error))
		{
			// Get response
			string requestResponse = request.text;
			
			// Check result
			Dictionary<string, object> requestResult = MiniJSON.Json.Deserialize(requestResponse) as Dictionary<string, object>;
			success = (bool)requestResult["success"];
			
			// Init list of printers
			if (success)
			{
				message = request.text;
				List<object> resultPrinters = (List<object>) requestResult["printers"];
				
				printer = GoogleCloudPrinter.Factory((Dictionary<string, object>) resultPrinters[0]);
			}
			
			if (storeResponse)
			{
				response = requestResponse;
			}
		}
		else
		{
			message = request.error;
		}
		
		// Call the callback
		callback(success, message, response, printer);
	}
}

/// <summary>
/// Google cloud job.
/// </summary>
public class GoogleCloudJob
{
	public string[] Tags { get; set; }
	
	public string CreateTime { get; set; }
	
	public string PrinterName { get; set; }
	
	public string UpdateTime { get; set; }
	
	public string Status { get; set; }
	
	public string OwnerId { get; set; }
	
	public string TicketUrl { get; set; }
	
	public string PrinterId { get; set; }
	
	public string PrinterType { get; set; }
	
	public string ContentType { get; set; }
	
	public string FileUrl { get; set; }
	
	public string Id { get; set; }
	
	public string Message { get; set; }
	
	public string Title { get; set; }
	
	public string ErrorCode { get; set; }
	
	public long NumberOfPages { get; set; }
	
	public static GoogleCloudJob Factory(Dictionary<string, object> jobData)
	{
		// Compute tags
		string[] tags = null;
		
		if (jobData.ContainsKey("tags"))
		{
			List<object> jobTags = (List<object>)jobData["tags"];
			tags = new string[jobTags.Count];
			
			int i = 0;
			foreach (object obj in jobTags)
			{
				tags[i++] = (string)obj;
			}
		}
		
		// Create new GoogleCloudJob object
		return new GoogleCloudJob()
		{
			Tags 					= tags,
			CreateTime 				= jobData.ContainsKey("createTime") 	? (string)jobData["createTime"] : null,
			PrinterName 			= jobData.ContainsKey("printerName") 	? (string)jobData["printerName"] : null,
			UpdateTime 				= jobData.ContainsKey("updateTime") 	? (string)jobData["updateTime"] : null,
			Status 					= jobData.ContainsKey("status") 		? (string)jobData["status"] : null,
			OwnerId 				= jobData.ContainsKey("ownerId") 		? (string)jobData["ownerId"] : null,
			TicketUrl 				= jobData.ContainsKey("ticketUrl") 		? (string)jobData["ticketUrl"] : null,
			PrinterId 				= jobData.ContainsKey("printerid") 		? (string)jobData["printerid"] : null,
			PrinterType 			= jobData.ContainsKey("printerType") 	? (string)jobData["printerType"] : null,
			ContentType 			= jobData.ContainsKey("contentType") 	? (string)jobData["contentType"] : null,
			FileUrl 				= jobData.ContainsKey("fileUrl") 		? (string)jobData["fileUrl"] : null,
			Id 						= jobData.ContainsKey("id") 			? (string)jobData["id"] : null,
			Message 				= jobData.ContainsKey("message") 		? (string)jobData["message"] : null,
			Title 					= jobData.ContainsKey("title") 			? (string)jobData["title"] : null,
			ErrorCode 				= jobData.ContainsKey("errorCode") 		? (string)jobData["errorCode"] : null,
			NumberOfPages 			= jobData.ContainsKey("numberOfPages") 	? (long)jobData["numberOfPages"] : 0,
		};
	}
}

/// <summary>
/// Google cloud printer.
/// </summary>
public class GoogleCloudPrinter
{
    public string Id { get; set; }

    public string Name { get; set; }

    public string Description { get; set; }

    public string Proxy { get; set; }

    public string Status { get; set; }

    public string CapsHash { get; set; }

    public string CreateTime { get; set; }

    public string UpdateTime { get; set; }

    public string AccessTime { get; set; }
	
	public string SupportedContentTypes { get; set; }
	
	public string OwnerId { get; set; }
	
	public string GcpVersion { get; set; }
	
	public bool IsTosAccepted { get; set; }
	
	public string Type { get; set; }
	
	public string ConnectionStatus { get; set; }
	
	public string DefaultDisplayName { get; set; }
	
	public string DisplayName { get; set; }
	
	public List<GoogleCloudPrinterCapabilities> Capabilities { get; set; }
	
	
	
	public static GoogleCloudPrinter Factory(Dictionary<string, object> printerData)
	{
		// Compute capabilities
		List<GoogleCloudPrinterCapabilities> capabilities = null;
		
		if (printerData.ContainsKey("capabilities"))
		{
			capabilities = new List<GoogleCloudPrinterCapabilities>();
			List<object> resultCapabilities = (List<object>) printerData["capabilities"];
				
			foreach (object capabilitiesObj in resultCapabilities)
			{
				Dictionary<string, object> capabilitiesData = (Dictionary<string, object>) capabilitiesObj;
				
				// Add the printer
				capabilities.Add(GoogleCloudPrinterCapabilities.Factory(capabilitiesData));
			}
		}
		
		return new GoogleCloudPrinter()
		{
			CreateTime 				= printerData.ContainsKey("createTime") 	? (string)printerData["createTime"] : null,
			AccessTime 				= printerData.ContainsKey("accessTime") 	? (string)printerData["accessTime"] : null,
			SupportedContentTypes 	= printerData.ContainsKey("supportedContentTypes") ? (string)printerData["supportedContentTypes"] : null,
			UpdateTime 				= printerData.ContainsKey("updateTime") 	? (string)printerData["updateTime"] : null,
			Status 					= printerData.ContainsKey("status") 		? (string)printerData["status"] : null,
			OwnerId 				= printerData.ContainsKey("ownerId") 		? (string)printerData["ownerId"] : null,
			GcpVersion 				= printerData.ContainsKey("gcpVersion") 	? (string)printerData["gcpVersion"] : null,
			CapsHash 				= printerData.ContainsKey("capsHash") 		? (string)printerData["capsHash"] : null,
			IsTosAccepted 			= printerData.ContainsKey("isTosAccepted") 	? (bool)printerData["isTosAccepted"] : false,
			Type 					= printerData.ContainsKey("type") 			? (string)printerData["type"] : null,
			Id 						= printerData.ContainsKey("id") 			? (string)printerData["id"] : null,
			ConnectionStatus 		= printerData.ContainsKey("connectionStatus") 	? (string)printerData["connectionStatus"] : null,
			Description 			= printerData.ContainsKey("description") 	? (string)printerData["description"] : null,
			Proxy 					= printerData.ContainsKey("proxy") 			? (string)printerData["proxy"] : null,
			Name 					= printerData.ContainsKey("name") 			? (string)printerData["name"] : null,
			DefaultDisplayName 		= printerData.ContainsKey("defaultDisplayName") ? (string)printerData["defaultDisplayName"] : null,
			DisplayName 			= printerData.ContainsKey("displayName") 	? (string)printerData["displayName"] : null,
			Capabilities			= capabilities
		};
	}
}

/// <summary>
/// Google cloud printer capabilities.
/// </summary>
public class GoogleCloudPrinterCapabilities
{
	public string Name { get; set; }
	
	public string Type { get; set; }
	
	public string PSFSelectionType { get; set; }
	
	public string PSFDataType { get; set; }
	
	public string PSFUnitType { get; set; }
	
	public string PSFMinValue { get; set; }
	
	public string PSFMaxValue { get; set; }
	
	public string PSFDefaultValue { get; set; }
	
	public string PSKDisplayName { get; set; }
	
    public List<Dictionary<string, string>> Options { get; set; }
	
	public static GoogleCloudPrinterCapabilities Factory(Dictionary<string, object> printerData)
	{
		// Compute options
		List<Dictionary<string, string>> options = null;
		
		if (printerData.ContainsKey("options"))
		{
			options = new List<Dictionary<string, string>>();
			List<object> resultOptions = (List<object>) printerData["options"];
				
			foreach (object optionObj in resultOptions)
			{
				Dictionary<string, string> optionGroup = new Dictionary<string, string>();
				Dictionary<string, object> optionData = (Dictionary<string, object>) optionObj;
				
				foreach (KeyValuePair<string, object> pair in optionData)
				{
					// Add the options
					optionGroup.Add(pair.Key, (string)pair.Value.ToString());
				}
				
				// Add group to list of options
				options.Add(optionGroup);
			}
		}
		
		return new GoogleCloudPrinterCapabilities()
		{
			Name 					= printerData.ContainsKey("name") 				? (string)printerData["name"] : null,
			Type 					= printerData.ContainsKey("type") 				? (string)printerData["type"] : null,
			PSFSelectionType 		= printerData.ContainsKey("psf:SelectionType") 	? (string)printerData["psf:SelectionType"] : null,
			PSFDataType 			= printerData.ContainsKey("psf:DataType") 		? (string)printerData["psf:DataType"] : null,
			PSFUnitType 			= printerData.ContainsKey("psf:UnitType") 		? (string)printerData["psf:UnitType"] : null,
			PSFMinValue 			= printerData.ContainsKey("psf:MinValue") 		? (string)printerData["psf:MinValue"] : null,
			PSFMaxValue 			= printerData.ContainsKey("psf:MaxValue") 		? (string)printerData["psf:MaxValue"] : null,
			PSFDefaultValue 		= printerData.ContainsKey("psf:DefaultValue") 	? (string)printerData["psf:DefaultValue"] : null,
			PSKDisplayName 			= printerData.ContainsKey("psk:DisplayName") 	? (string)printerData["psk:DisplayName"] : null,
			Options 				= options
		};
	}
}