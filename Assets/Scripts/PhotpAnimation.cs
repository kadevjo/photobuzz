﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class PhotpAnimation : MonoBehaviour {

	[SerializeField]
	private UISprite filledSprite;
	[SerializeField]
	private float animationTime=3f;
	[SerializeField]
	private Game game;
	[SerializeField]
	private AudioClip beepSound;

	public void TakePhoto(){
		StartCoroutine("AnimationPhoto");
	}

	IEnumerator AnimationPhoto(){
		var buttons = game.hideableButtons.Skip( 3 ).ToList();
		buttons.ForEach( b => b.SetActive( false ) );

		float actualTime = 0f;
		//Animation
		while(actualTime < this.animationTime + 0.1f){
			actualTime +=Time.deltaTime;
			filledSprite.fillAmount = 1 - (actualTime/this.animationTime);
			if((int)(actualTime*10)%10 == 0) audio.Play();
			yield return null;
		}
		//Call the function for take the photo
		game.TakePhoto();
		yield return new WaitForSeconds(1f);
		//Restet the fill
		filledSprite.fillAmount = 1;
	}


}
