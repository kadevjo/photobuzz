﻿using UnityEngine;
using System.Collections;
using System;

public class LoadingWindow : Window<LoadingWindow> {

	public static void Show(int seconds){
		if(!isRunning){
			_Show("LoadingWindow");
			Instance.Initialize();
			Destroy( Instance.gameObject, seconds );
			isRunning = false;
		}
	}

}
