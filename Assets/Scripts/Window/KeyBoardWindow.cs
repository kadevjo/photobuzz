﻿using UnityEngine;
using System.Collections;
using System;

public class KeyBoardWindow : Window<KeyBoardWindow> {
	[SerializeField]
	private UILabel targetLabel;
	Action<string> Success;
	public static void Show(Action<string> OnSuccess){
		if(!isRunning){
			_Show("KeyBoardWindow");
			Instance.Initialize();
			Instance.Success+= OnSuccess;
		}
	}

	public void Aceptar(){
		Success(targetLabel.text.ToLower());
		Close();
	}

	protected override void BeforeClose ()
	{
		//Clear Callback
		Success = null;
	}
	public void Cancelar(){
		Close();
	}

}
