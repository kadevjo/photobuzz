﻿using UnityEngine;
using System.Collections;
[ExecuteInEditMode]
public class KeyScript : MonoBehaviour {

   [SerializeField]
	UILabel keyLabel;

	[SerializeField]
	UILabel targetLabel;

	void Awake(){
		if(keyLabel != null) keyLabel.text = this.name;
	}

	public void KeyPress(){
		targetLabel.text += this.name;
	}

	public void Delete(){
		string text = targetLabel.text;
		if(text.Length > 0){
			targetLabel.text = text.Substring(0, text.Length -1);
		}
	}
}
