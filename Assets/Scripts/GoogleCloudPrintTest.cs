﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[RequireComponent(typeof(GoogleAuthentication))]
[RequireComponent(typeof(GoogleCloudPrintPlugin))]
public class GoogleCloudPrintTest : MonoBehaviour 
{	
	//Guanapolio iOS Keys
	private static string AppClientId = "575653175549-mjmrqfvoqe650s2kfvnarm0n6f4qekgq.apps.googleusercontent.com";
	private static string AppClientSecret = "LEcaU3Fv6Gq48nM2LeDRkozH";


	private static string REDIRECT_URI = "urn:ietf:wg:oauth:2.0:oob";
	private static string SCOPE = "https://www.googleapis.com/auth/cloudprint";
	private static string APP_PROXY = "com.kadevjo.guanapolio";
	
	private string resultText = "";
	private string logText = "";

	private string googleAccountLogin = "kadevjo@gmail.com";
	private string googleAccountPassword = "#4d3vj057ud10";
	
	private string clientId = "";
	private string authCode = "";
	private string authToken = "";
	
	private int currentTextureId = 0;
	private Texture2D texture;
	
	private GoogleCloudPrintPlugin GCPPlugin;
	private GoogleAuthentication googleAuthentication;
	private List<GoogleCloudPrinter> printers;
	private List<GoogleCloudJob> currentJobs;

	[SerializeField]
	private bool showGui = true;
	
	[SerializeField]
	private string nextScene;
	// Use this for initialization
	void Start () 
	{
		// Retrieve plugin component
		googleAuthentication = GetComponent<GoogleAuthentication>();
		GCPPlugin = GetComponent<GoogleCloudPrintPlugin>();
		
		// Init plugin
		GCPPlugin.AppProxy = APP_PROXY;
		
		// Init printers and jobs list
		printers = new List<GoogleCloudPrinter>();
		currentJobs = new List<GoogleCloudJob>();

		DontDestroyOnLoad(this);
		// Load first texture 
		//texture = (Texture2D)Resources.Load("Textures/MyTexture" + currentTextureId);
	}
	
	#if UNITY_ANDROID
	public void AuthTokenCallback(string result)
	{
		Dictionary<string, object> requestResult = MiniJSON.Json.Deserialize(result) as Dictionary<string, object>;
		bool success  = (bool)requestResult["success"];
		
		if (success)
		{
			authToken = (string)requestResult["authtoken"];
			resultText = result;
		}
		else
		{
			resultText = (string)requestResult["message"];
		}
	}
	#endif
	
	private void OnGUI()
	{
		if(!showGui) return;
		// Google Client Connection
		GUI.Label(new Rect(10, 10, 500, 20), "Google Client Connection");
		GUI.Label(new Rect(10, 40, 150, 20), "Login : ");
		GUI.Label(new Rect(10, 70, 150, 20), "Password : ");
		GUI.Label(new Rect(10, 100, 150, 20), "Client Id : " + (clientId.Length > 0 ? "OK" : "NO"));
		
		googleAccountLogin = GUI.TextField(new Rect(100, 30, 150, 20), googleAccountLogin);
		googleAccountPassword = GUI.PasswordField(new Rect(100, 60, 150, 20), googleAccountPassword, '*');
		
		if (GUI.Button(new Rect(10, 130, 200, 30), "GCP Get CLient Login"))
		{
			googleAuthentication.ClientLogin("HOSTED_OR_GOOGLE", googleAccountLogin, googleAccountPassword, "cloudprint", APP_PROXY, (success, message, SID, LSID, auth) => 
			                                 {
				if (success)
				{
					// Store the client auth
					clientId = auth;
					
					// Reset the auth token
					authToken = "";
					
					// Update authorization type
					GCPPlugin.AuthorizationType = GoogleCloudPrintPlugin.GCPAuthorizationType.GoogleLogin;
				}
				else
				{
					resultText = message;
				}
			});
		}
		
		// OAuth2 Connection
		GUI.Label(new Rect(300, 10, 500, 20), "OAuth2 Connection");
		
		#if !UNITY_ANDROID
		GUI.Label(new Rect(300, 40, 150, 20), "OAuth Code : ");
		#endif
		
		GUI.Label(new Rect(300, 70, 150, 20), "OAuth Token : " + (authToken.Length > 0 ? "OK" : "NO"));
		
		#if !UNITY_ANDROID
		authCode = GUI.TextField(new Rect(390, 40, 150, 20), authCode);
		
		if (GUI.Button(new Rect(550, 35, 140, 30), "Request Code"))
		{
			googleAuthentication.OAuth2Code("code", AppClientId, REDIRECT_URI, SCOPE);
		}
		#endif
		
		if (GUI.Button(new Rect(300, 130, 200, 30), "GCP Get OAUth2 Token"))
		{
			#if UNITY_ANDROID
			// Get a OAuth2 Token for Google Cloud Print services
			googleAuthentication.GetAuthTokenByFeatures("com.google", "oauth2:" + SCOPE, "AuthTokenCallback", gameObject);
			#else
			googleAuthentication.OAuth2Token(authCode, AppClientId, AppClientSecret, REDIRECT_URI, "authorization_code", SCOPE, (success, message, accessToken, tokenType, expiresIn, refreshToken) => 
			                                 {
				if (success)
				{
					// Store the auth token
					authToken = accessToken;
					
					// Reset the client auth
					clientId = "";
					
					// Update authorization type
					GCPPlugin.AuthorizationType = GoogleCloudPrintPlugin.GCPAuthorizationType.OAuth;
				}
				else
				{
					resultText = message;
				}
			});
			#endif
		}
		
		if (clientId.Length > 0 || authToken.Length > 0)
		{
			if (GUI.Button(new Rect(10, 220, 200, 30), "GCP Search"))
			{
				GCPPlugin.Search(clientId.Length == 0 ? authToken : clientId, 
				                 (success, message, response, printers) => 
				                 {
					// Save the printers
					this.printers = printers;
					
					// Log response
					logText = response;
				}, true);
			}
			
//			// Show Labels
//			GUI.Label(new Rect(10, 255, 100, 30), "SUBMIT TO");
//			GUI.Label(new Rect(10, 295, 100, 30), "JOBS FROM");
//			GUI.Label(new Rect(10, 335, 100, 30), "CAPABALITIES");
//			
//			int i = 0;
			int printersCount = this.printers.Count;		
			// Show printers count
			if (printersCount > 0)
			{
				GUI.Label(new Rect(250, 220, 250, 30), printersCount + " printers founded. 3 printers displayed.");
				Application.LoadLevelAsync(this.nextScene);
				this.showGui = false;
			}
			/*
			// Show only 3 printers
			for (; i < Mathf.Min(3, printersCount) ; ++i)
			{
				GoogleCloudPrinter printer = printers[i];
				
				for (int j = 0; j < 3; ++j)
				{
					if (GUI.Button(new Rect(110 + (170 * i), 255 + (j * 40), 160, 30), printer.Name))
					{
						if (j == 0)
						{
							GCPPlugin.Submit(clientId.Length == 0 ? authToken : clientId,
							                 printer.Id,
							                 texture.name,
							                 "",
							                 texture.EncodeToPNG(),
							                 "image/png",
							                 "",
							                 (success, message, response) => 
							                 {
								// Show the result message
								resultText = message;
								
								// Log response
								logText = response;
							}, true);
						}
						else if (j == 1)
						{
							GCPPlugin.Jobs(clientId.Length == 0 ? authToken : clientId,
							               printer.Id,
							               (success, message, response, jobs) => 
							               {
								// Show Jobs count
								resultText = jobs.Count + " Jobs";
								
								// Store jobs
								currentJobs = jobs;
								
								// Log response
								logText = response;
							}, true);
						}
						else if (j == 2)
						{
							GCPPlugin.Printer(clientId.Length == 0 ? authToken : clientId,
							                  printer.Id,
							                  true,
							                  (success, message, response, printerCapabilities) => 
							                  {
								// Show Capicities count
								resultText = printerCapabilities.Capabilities.Count + " Capacities";
								
								// Log response
								logText = response;
							}, true);
						}
					}
				}
			}
			*/
			/*
			i = 0;
			foreach (GoogleCloudJob job in this.currentJobs)
			{
				if (GUI.Button(new Rect(10 + (180 * i), 380, 170, 30), "DEL JOB " + job.Title))
				{
					GCPPlugin.DeleteJob(clientId.Length == 0 ? authToken : clientId,
					                    job.Id,
					                    (success, message, response) => 
					                    {
						// Show Jobs
						resultText = message;
						
						// clear jobs
						currentJobs.Clear();
						
						// Log response
						logText = response;
					}, true);
				}
				
				++i;
			}*/
		}
		/*
		// Texture button
		if (GUI.Button(new Rect(10, 170, 100, 40), texture))
		{
			ChangeTexture();
		}
		
		// Result and log text
		resultText = GUI.TextArea(new Rect(10, 440, 300, 70), resultText);
		logText = GUI.TextArea(new Rect(320, 440, 300, 70), logText);
		*/
	}
	
	private void ChangeTexture()
	{
		// Load next texture
		if (currentTextureId < 3)
		{
			++currentTextureId;
		}
		else
		{
			currentTextureId = 0;
		}
		
		texture = (Texture2D)Resources.Load("Textures/MyTexture" + currentTextureId);
	}
}