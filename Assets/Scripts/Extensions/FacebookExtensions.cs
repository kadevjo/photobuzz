﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
//// Retrieves the scores for your app
//public void getScores( string userId, Action<string, object> onComplete )
//{
//	var path = userId + "/scores";
//	graphRequest( path, onComplete );
//}

public static class FacebookExtensions {

	public static void getAchievements(this Facebook facebook, string userId, Action<string,object> onComplete){
		var path = userId + "/achievements";
		facebook.graphRequest(path,onComplete);
	}

	public static void getCompleteAchievements(this Facebook facebook, string appId,Action<string,object> onComplete){
		var path = string.Format("me/achievements?fields=data&app_id_filter={0}",appId);
		facebook.graphRequest(path,onComplete);
	}

	public static void getMyScores( this Facebook facebook ,Action<string, object> onComplete )
	{
		var path = "me/scores?fields=application.fields(id),score";
		facebook.graphRequest( path, onComplete );
	}

	
	public static void postAchievement(this Facebook facebook , string url, Action<bool> completionHandler){

		Debug.Log ("Url : " + url);
		var parameters = new Dictionary<string,object>()
		{
			{ "achievement", url }
		};

		facebook.post( "/me/achievements", parameters, ( error, obj ) =>
		     {
			if( error == null && obj is string )
			{
				completionHandler( ((string)obj).ToLower() == "true" );
			}
			else
			{
				Debug.Log(error);
			}
		});
	}
}
