﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Prime31;

public class Game : MonoBehaviour {

	WebCamTexture videoTexture;
	public UITexture texture;
	/// <summary>
	/// The album identifier.
	/// </summary>
	[SerializeField]
	private string albumId = "";
	/// <summary>
	/// The name of the image.
	/// </summary>
	[SerializeField]
	private string imageName = "foto.png";
	/// <summary>
	/// The base path.
	/// </summary>
	private string basePath  = "";
	/// <summary>
	/// The camera enable.
	/// </summary>
	[SerializeField]
	private bool cameraEnable;
	[SerializeField]
	private string fbAccessToken = "";
	// Use this for initialization

	[SerializeField] List<GameObject> frames; 

	public UITexture photo;

	public List<GameObject> hideableButtons;
	public GameObject shareContainer;

	public GameObject photoContainer;

	public GoogleCloudManager GCM;

	//Buttons
	public UIButton printButton;
	public UIButton facebookButton;
	public UIButton mailButton;

	byte[] image;

	void Start () {
		//For use the camera of the PC and show it on the scene
	if(cameraEnable){
		videoTexture = new WebCamTexture( WebCamTexture.devices[ 0 ].name, 1024, 1536 );
		WebCamTexture.devices.ToList().ForEach( d => Debug.Log( d.name ) );
		texture.material.mainTexture = videoTexture;
		videoTexture.Play();
	}
		//For get the data of the basepath
		this.basePath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Desktop);

		GCM.ConnectPrinter();

		//Set the access token to publish the image in windows
		Facebook.instance.accessToken = this.fbAccessToken;
		/*Facebook.instance.extendAccessToken(
			"1435831970001793",
			"6aa1ea83ac91d46c7ad7428f9de16da4",
			( dt ) => {
				Debug.Log( "Extended " + dt.ToString() );
			}
		);*/
	}

	public void TakePhoto () {
		StartCoroutine(Take ());
	}

	private IEnumerator Take () {


		//Hide Button
		hideableButtons.ForEach( b => b.SetActive(false) );
		audio.Play();
		yield return new WaitForSeconds(0.5f);
		//Setting the path
		this.imageName = System.Guid.NewGuid().ToString() + ".png";
		string path = System.IO.Path.Combine(this.basePath,this.imageName);
		Application.CaptureScreenshot(path,2);
		//Wait for the file to be save
		LoadingWindow.Show( 4 );
		yield return new WaitForSeconds(5f);
		WWW img = new WWW("file://"+path);
		while(!img.isDone){
			yield return null;
		}

		photo.mainTexture = img.texture;
		photoContainer.SetActive(false);
		shareContainer.SetActive(true);

		Texture2D t = ScaleTexture( photo.mainTexture as Texture2D, 600, 900 );
		image = t.EncodeToPNG();
		System.IO.File.WriteAllBytes(path,image);
	}

	public void Back(){
		mailButton.isEnabled = true;
		facebookButton.isEnabled = true;
		printButton.isEnabled = true;

		shareContainer.SetActive(false);
		hideableButtons.ForEach( b => b.SetActive(true) );
		photoContainer.SetActive(true);
	}

	public void SendEmail () {
		mailButton.isEnabled = false;

		KeyBoardWindow.Show((text)=>{
			string imagePath = System.IO.Path.Combine(this.basePath,this.imageName);
			if( !System.IO.File.Exists( imagePath ) )
			{
				Debug.LogError( "there is no screenshot avaialable at path: " + imagePath );
				return;
			}
			
			Email.Send(imagePath,text);
			LoadingWindow.Show( 5 );
		});
	}

	public void PubishToFacebook (){
		facebookButton.isEnabled = false;

		var message = "Mi Foto!";
		LoadingWindow.Show( 5 );
		Facebook.instance.postImageToAlbum( image, message,this.albumId,(s,o)=>{
			Debug.Log(o.ToString());
		});

		/*Facebook.instance.postImage(albumId,image,message,(s,o)=>{
			Debug.Log(o.ToString());
		});*/
	}

	public void PrintPhoto(){
		printButton.isEnabled = false;

		Debug.Log("PrintingPress");
		/*string imagePath = System.IO.Path.Combine(this.basePath,this.imageName);
		if( !System.IO.File.Exists( imagePath ) )
		{
			Debug.LogError( "there is no screenshot avaialable at path: " + imagePath );
			return;
		}
		//Get bytes for send it to facebbok
		var image = System.IO.File.ReadAllBytes( imagePath );
		GoogleCloudManager.Instance.PrintImage( image);*/

		LoadingWindow.Show( 5 );
		PrintPluginDemo.Print(photo.mainTexture as Texture2D);

	}

	private Texture2D ScaleTexture(Texture2D source,int targetWidth,int targetHeight) {
		Texture2D result=new Texture2D(targetWidth,targetHeight,source.format,true);
		Color[] rpixels=result.GetPixels(0);
		float incX=((float)1/source.width)*((float)source.width/targetWidth);
		float incY=((float)1/source.height)*((float)source.height/targetHeight);
		for(int px=0; px<rpixels.Length; px++) {
			rpixels[px] = source.GetPixelBilinear(incX*((float)px%targetWidth),
			                                      incY*((float)Mathf.Floor(px/targetWidth)));
		}
		result.SetPixels(rpixels,0);
		result.Apply();
		return result;
	}

	public void SelectFrame1 () {
		frames.ForEach( f => f.SetActiveRecursively( false ) );
		frames[ 0 ].SetActiveRecursively( true );
	}

	public void SelectFrame2 () {
		frames.ForEach( f => f.SetActiveRecursively( false ) );
		frames[ 1 ].SetActiveRecursively( true );
	}

	public void SelectFrame3 () {
		frames.ForEach( f => f.SetActiveRecursively( false ) );
		frames[ 2 ].SetActiveRecursively( true );
	}

	public void SelectFrame4 () {
		frames.ForEach( f => f.SetActiveRecursively( false ) );
		frames[ 3 ].SetActiveRecursively( true );
	}

	public void Quit () {
		Application.Quit();
	}
}
