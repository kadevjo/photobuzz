using UnityEngine;
using System.Collections;
using System;
using System.Runtime.InteropServices;

public class PrintPluginDemo {

	enum PrintScaleMode : int{
		NO_SCALE = 0,
		PAGE_WIDTH,
		PAGE_HEIGHT,
		FILL_PAGE
	}
	
	// Texture MUST be uncompressed 24bit or 32bit and read/write enabled
	public Texture2D textureToPrint;
	
	[DllImport ("PrintPlugin")]
	private static extern int PrintTexture(System.IntPtr texture, bool showDialog, int printScaleMode);

	public static void Print ( Texture2D textureToPrint ) {

		// Send texture to default printer, print dialog disableds
		PrintTexture(textureToPrint.GetNativeTexturePtr(), false, (int)PrintScaleMode.FILL_PAGE);
	}
}
